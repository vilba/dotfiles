#!/bin/ksh

echo "$(cat /etc/slackware-version) $(uname -m) on $(uname -s) $(uname -r)"

PS1="$(hostname -s)) "

EDITOR='gvim'
VISUAL="$EDITOR"
BROWSER='firefox'

stty -ixon

set -o emacs

alias vi='vim'

alias rm='rm -Iv'
alias cp='cp -iv'
alias mv='mv -iv'

alias ls='ls --color=auto'
alias la='ls -la'
alias ll='ls -l'

alias tree='tree -C'
alias t='tree'
alias ta='tree -la'
alias tt='tree -l'
