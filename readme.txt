.dotfiles - vilba's twm confs
=============================

details
-------
+ OS (that i use): Slackware
+ Terminal emulator: XTerm
+ Editor: GViM
+ Shell: KSH
+ Web browser: Firefox
+ Window manager: TWM (yes, *that* twm..)
+ Color scheme: Custom; koehler in Vi
+ Compositor: XCompmgr

deploy (using GNU stow)
-----------------------

    .dotfiles$ stow -nv .
