syntax on
filetype plugin indent on
colors koehler

set guioptions=
set background=dark
set guifont=monospace\ 10
set ruler
set history=512
set mouse=
set confirm
set number
set relativenumber
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set wildmenu
set smarttab
set autoindent
set smartindent
set nobackup
set nowritebackup
set undodir=/tmp
